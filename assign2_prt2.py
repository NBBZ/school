# D. verbing
# Given a string, if its length is at least 3,
# add 'ing' to its end.
# Unless it already ends in 'ing', in which case
# add 'ly' instead.
# If the string length is less than 3, leave it unchanged.
# Return the resulting string.
def verbing(str_input):
    if len(str_input) < 3:
        return str_input
    if str_input[-3:] == 'ing':
        return str_input + 'ly'
    return str_input + 'ing'

# E. not_bad
# Given a string, find the first appearance of the
# substring 'not' and 'bad'. If the 'bad' follows
# the 'not', replace the whole 'not'...'bad' substring
# with 'good'.
# Return the resulting string.
# So 'This dinner is not that bad!' yields:
# This dinner is good!
def not_bad(str_input):
    # had to use this stupid method to make sure the locations were defined b4 trying to compare them
    # couldnt think of a way to make it cleaner
    not_n, bad_b = None, None
    if 'not' in str_input and 'bad' in str_input:
        for i in range(len(str_input)):
            if str_input[i:i+3] == 'not':
                not_n = i
            if str_input[i:i+3] == 'bad':
                bad_b = i
            if not_n != None and bad_b != None and bad_b > not_n:
                return str_input[:not_n] + 'good' + str_input[bad_b+3:]
    return str_input

# F. front_back
# Consider dividing a string into two halves.
# If the length is even, the front and back halves are the same length.
# If the length is odd, we'll say that the extra char goes in the front half.
# e.g. 'abcde', the front half is 'abc', the back half 'de'.
# Given 2 strings, input1 and input2, return a string of the form
#  input1-front + input2-front + input1-back + input2-back
def front_back(str_input1, str_input2):
    front1 = str_input1[:len(str_input1)//2 + len(str_input1)%2]
    front2 = str_input2[:len(str_input2)//2 + len(str_input2)%2]
    back1 = str_input1[len(str_input1)//2 + len(str_input1)%2:]
    back2 = str_input2[len(str_input2)//2 + len(str_input2)%2:]
    return front1 + front2 + back1 + back2



def test(got, expected):
    """ simple test() function used in main() to print
        what each function returns vs. what it's supposed to return. """
    if got == expected:
        prefix = ' OK '
    else:
        prefix = '  X '
    print ("%s got: %s expected: %s" % (prefix, repr(got), repr(expected)))


def main():
    """ main() calls the above functions with interesting inputs,
        using test() to check if each result is correct or not. """

    print ("\nverbing")
    test(verbing('hail'), 'hailing')
    test(verbing('swiming'), 'swimingly')
    test(verbing('do'), 'do')

    print ("\nnot_bad")
    test(not_bad('This movie is not so bad'), 'This movie is good')
    test(not_bad('This dinner is not that bad!'), 'This dinner is good!')
    test(not_bad('This tea is not hot'), 'This tea is not hot')
    test(not_bad("It's bad yet not"), "It's bad yet not")

    print ("\nfront_back")
    test(front_back('abcd', 'xy'), 'abxcdy')
    test(front_back('abcde', 'xyz'), 'abcxydez')
    test(front_back('Kitten', 'Donut'), 'KitDontenut')


if __name__ == '__main__':
    main()
