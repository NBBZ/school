import curses
import time
stdscr = curses.initscr()
curses.noecho() # dont echo the typed char to screen
curses.curs_set(False) # hide curser
screen_width = stdscr.getmaxyx()[1]
screen_height = stdscr.getmaxyx()[0]

stdscr.clear()
stdscr.addstr(int(screen_height/2), int(screen_width/2), 'WELCOME TO PONG')
stdscr.addstr(int(screen_height/2)+1, int(screen_width/2), 'D-F   J-K')
stdscr.getch()
stdscr.refresh()
stdscr.nodelay(True)

# starting pos for panels
right_panel = int(screen_height/2)
left_panel = int(screen_height/2)
panel_len = int(screen_height/4.3)

speed = 0.04

# BALL MOVEMENT EXPLANATION:
# implementing linear movement in discrete units (bassically meaning "pixel" in this case bc there are no half-pixels and i want each x,y to correspond to one actual pixel)
# is easy if were only using directions of n*45 degrees (just north-south-east-west)
# but i wanted to challange myself to make movement on any gradient in pixel form
# and that is problematic, if we look at the function of 0.5x, when mapping it into pixel it goes right --> right --> up-right
#        @@        @
#0.5x  @@     1x  @
#    @@          @ 
# but how do we know when to move up-right and when to move only right?
# i was messing with desmos when trying to figure this out and figured out that for any point x=a, the expression: floor(f(a+1)) - floor(f(a))
# basically means "does f go up a pixel at that particular x value when mapping it into a discrete pixel grid"
# but we still need a refrence point to that x, to know if its a "right" pixel or a "right-up" pixel
# so we define a x value called "origin" which is bassically the x=0 of that function
# and that way we can calculate for each x value if the next pixel is to its right, right-up, or right-down
# (yes i could have avoided all of that by just using floating-point x,y values and then rounding them when drawing but where is the fun in that)

grad= 0.5
direction = 1
posX, posY = 3, int(screen_height/2) # starting ball pos
origin = posX
# serve system just to make the game easier, after someome made a point, another point wont be made until one of the player touches the ball
serve = 0
def moveByDir(grad, x, y):
    a = abs(x+1 - origin)
    y += int((a+1)*grad) - int(a*grad)
    x += direction
    return x,y

t_end = time.time() + 60
while time.time() < t_end:
    #screen_width = stdscr.getmaxyx()[1]
    #screen_height = stdscr.getmaxyx()[0]
    posX, posY = moveByDir(grad, posX, posY)
    stdscr.addstr(posY, posX, '@')
    # getting input and making sure panels dont go off-screen
    inp = stdscr.getch()
    if inp == 106 and right_panel+panel_len < screen_height-1: right_panel += 1
    if inp == 107 and right_panel > 0: right_panel -= 1
    if inp == 102 and left_panel+panel_len < screen_height-1: left_panel += 1
    if inp == 100 and left_panel > 0: left_panel -= 1
    # drawing panels
    for i in range(panel_len): 
        stdscr.addstr(int(left_panel+i), 0, '#')
        stdscr.addstr(int(right_panel+i), screen_width-1, '#')
    # panel collision
    if posX == screen_width-3 and posY >= right_panel and posY <= right_panel+panel_len or \
       posX == 2 and posY >= left_panel and posY <= left_panel+panel_len:
        #if posY == right_panel or posY == left_panel or posY == right_panel+panel_len or posY == left_panel+panel_len: grad= dir*2
        #if posY == right_panel+panel_len/2 or posY == left_panel+panel_len/2: grad= dir*0.5
        origin = posX
        direction = -direction
        serve = 0
        speed = speed*0.9
    # X axis walls collision (y==top of screen or bottom)
    if posY == screen_height-1 or posY == 1:
        origin = posX
        grad= -grad
    # Y axis walls collision
    if posX == screen_width-1 or posX == 1:
        # if a point has been made and a player hasnt yet touched the ball
        if serve == 1:
            origin = posX
            direction = -direction
        # a point has been made:
        else:
            serve = 1
            stdscr.clear()
            stdscr.addstr(int(screen_height/2), int(screen_width/2), 'WAIT')
            stdscr.refresh()
            time.sleep(0.5)
            posX, posY = int(screen_width/2), int(screen_height/2)
    stdscr.refresh()
    stdscr.clear()
    time.sleep(speed)



# curses <3
curses.nocbreak()
stdscr.keypad(False)
curses.echo()
curses.endwin()
