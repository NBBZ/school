def grades(a):
    """manipulate list of names and grades as specified"""
    # simply iterating on every other item and appending as needed
    for i in range(0,len(a),2):
        avg = sum(a[i+1])/len(a[i+1])
        a[i].append(avg)
        if avg > 90:
            a[i].append('Excellent')
        elif avg > 55:
            a[i].append('Pass')
        else:
            a[i].append('Fail')
        return a

def mostMoney(a):
    """finds and prints info on the movie which made the most money"""
    # sorting by second element in the sublist and simply printing
    # I could add formating if I wanted but im lazy
    print(sorted(a, key = lambda x: x[2], reverse=True)[0])

def gradeInput():
    studentArr = []
    for i in range(1, 6):
        name = input(f'enter name for student number {i} --> ') 
        studentArr.append(name)
        gradeArr = []
        count = 1
        grade = int(input(f'enter grade {count} for student number {i} --> '))
        while grade != -99:
            count += 1
            gradeArr.append(grade)
            grade = int(input(f'enter grade {count} for student number {i} --> '))
        studentArr.append(gradeArr)
    for i in range(0,len(studentArr),2):
        print(studentArr[i], end=' ')
        print(studentArr[i+1])

gradeInput()
