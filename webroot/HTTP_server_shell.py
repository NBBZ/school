# HTTP Server Shell
# Author: Barak Gonen
# Purpose: Provide a basis for Ex. 4.4
# Note: The code is written in a simple way, without classes, log files or other utilities, for educational purpose
# Usage: Fill the missing functions and constants

# TO DO: import modules
import socket
import os

# TO DO: set constants
SOCKET_TIMEOUT = 10
PORT = 4444
IP = '127.0.0.1'
WEBROOT = '/home/nbbz/school/webroot'
DEFAULT_URL = '/home/nbbz/school/webroot/index.html'


def get_file_data(filename):
    """ Get data from file """
    if filename != DEFAULT_URL:
        filepath = WEBROOT + filename
    else: filepath = DEFAULT_URL
    print(filepath)
    if not os.path.isfile(filepath):
        print('SOURCE FILE OR DESTINATION FILE DOES NOT EXIST')
        return '404', b''
    if filepath == WEBROOT + '/FORBIDDEN.txt':
        print('FORBIDDEN')
        return '403', b''
    source = open(filepath, 'br')
    data = source.read()
    source.close()
    return '200', data



def handle_client_request(resource, client_socket):
    """ Check the required resource, generate proper HTTP response and send to client"""
    if 'calculate-next' in resource:
        data = int(resource.split('=')[1]) + 1
        data = str(data).encode()
        code = '200'
    elif 'calculate-area' in resource:
        data = resource.split('?')[1].split('&')
        data = int(data[0].split('=')[1]) * int(data[1].split('=')[1]) / 2
        data = str(data).encode()
        code = '200'
    elif resource == '' or resource == '/':
        url = DEFAULT_URL
        print('URL IS --> ' + url)
        code, data = get_file_data(url)
        filename, filetype = os.path.splitext(url)
    else:
        url = resource
        print('URL IS --> ' + url)
        code, data = get_file_data(url)
        filename, filetype = os.path.splitext(url)

    ###    # TO DO: check if URL had been redirected, not available or other error code. For example:
    ###    if url in REDIRECTION_DICTIONARY:
    ###        # TO DO: send 302 redirection response

    #print('FILETYPE IS --> ' + filetype)
    if code == '200': 
        http_header = b'HTTP/1.1 200 OK\r\n\r\n'
    elif code == '404':
        http_header = b'HTTP/1.1 404 Not Found\r\n\r\n'
        data = b'404 NOT FOUND'
    elif code == '403':
        http_header = b'HTTP/1.1 403 Forbidden\r\n\r\n'
        data = b'403 FORBIDDEN'
    elif filetype == '.html':
        http_header = b'HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=utf-8\r\nContent-Length: ' + b'str(len(data))\r\n\r\n'
    elif filetype == '.js':
        http_header = b'HTTP/1.1 200 OK\r\nContent-Type: text/javascript; charset=UTF-8\r\nContent-Length: ' + b'str(len(data))\r\n\r\n'
    elif filetype == '.css':
        http_header = b'HTTP/1.1 200 OK\r\nContent-Type: text/css\r\nContent-Length: ' + b'str(len(data))\r\n\r\n'
    elif filetype == '.jpg':
        http_header = b'HTTP/1.1 200 OK\r\nContent-Type: image/png\r\nContent-Length: ' + b'str(len(data))\r\n\r\n'
    else: 
        http_header = b'HTTP/1.1 500 Internal Server Error\r\n\r\n'
        data = b'500 ERROR'
    # TO DO: handle all other headers

    http_response = http_header + data
    client_socket.send(http_response)


def validate_http_request(request):
    """ Check if request is a valid HTTP request and returns TRUE / FALSE and the requested URL """
    req = request.split('\r\n')[0]
    if 'GET' not in req or 'HTTP/1.1' not in req: return False, ''
    resource = req.split()[1]
    return True, resource


def handle_client(client_socket):
    """ Handles client requests: verifies client's requests are legal HTTP, calls function to handle the requests """
    print('Client connected')
    while True:
        client_request = client_socket.recv(4096).decode()
        #print(client_request)
        valid_http, resource = validate_http_request(client_request)
        if valid_http:
            print('Got a valid HTTP request')
            handle_client_request(resource, client_socket)
        else:
            print('Error: Not a valid HTTP request')
        break
    print('Closing connection')
    client_socket.close()


def main():
    # Open a socket and loop forever while waiting for clients
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((IP, PORT))
    server_socket.listen(10)
    print("Listening for connections on port %d" % PORT)

    while True:
        client_socket, client_address = server_socket.accept()
        print('New connection received')
        client_socket.settimeout(SOCKET_TIMEOUT)
        handle_client(client_socket)


if __name__ == "__main__":
    # Call the main handler function
    main()
