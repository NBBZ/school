#
#
# TAL AND NISAN
#
#
import time
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

product = input('enter search query -> ')
max_price = int(input('enter max price -> '))
source = open( './shopping.txt', 'a')

# getting into the site (using instructions from presentation provided with the assignment)
browser = webdriver.Chrome()
browser.get('https://ebay.com/')
search_bar = browser.find_element(By.XPATH, '/html/body/div[3]/div/header/table/tbody/tr/td[3]/form/table/tbody/tr/td[1]/div[1]/div/input[1]')
search_bar.click()
search_bar.send_keys(product)
search_bar.send_keys(Keys.ENTER)

# finding all tags of "item info" which contain price, name, and link inside them
# the [1:] is because for some reason the first tag is a hidden item with a price of $20 which breaks the search
soup = BeautifulSoup(browser.page_source, 'html.parser')
items = soup.find_all('div', 's-item__info clearfix')[1:]

time.sleep(10)

# going over the items and getting their price, name, and url
# if price is less than max and is a valid float we add the info to the file
for i in items:
    price = i.find('span', 's-item__price').text.split()[1]
    name = i.find('span', role='heading').text
    link = i.find('a', 's-item__link', href=True)['href']
    if price.replace('.', '').isnumeric() and float(price) <= max_price:
        source.write(name + '\nPRICE: ' + price + '\n' + link + '\n------------------\n')
source.close
