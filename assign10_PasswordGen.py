#  _   _ ___ ____    _    _   _      _    _   _ ____
# | \ | |_ _/ ___|  / \  | \ | |    / \  | \ | |  _ \
# |  \| || |\___ \ / _ \ |  \| |   / _ \ |  \| | | | |
# | |\  || | ___) / ___ \| |\  |  / ___ \| |\  | |_| |
# |_| \_|___|____/_/   \_\_| \_| /_/   \_\_| \_|____/
#
#     _    ____    _    __  __
#    / \  |  _ \  / \  |  \/  |
#   / _ \ | | | |/ _ \ | |\/| |
#  / ___ \| |_| / ___ \| |  | |
# /_/   \_\____/_/   \_\_|  |_|

import random
import string
import os


def full_words(length, path):
    with open(path, "r") as file:
        words = file.read().splitlines()
    password = ""
    while True:
        word = random.choice(words)
        if len(password) + len(word) > length:
            continue
        elif len(password) + len(word) == length:
            return password + word
        else:
            password += word


length = input('enter length --> ')
if not length.isnumeric(): exit()
length = int(length)
if length < 1: exit()

passType = input('choose type: words or chars --> ')
if passType != 'chars' and passType != 'words': exit()

if passType == 'chars':
    password = ''.join(random.choices(string.digits + string.ascii_letters, k=length))
    print(password)

if passType == 'words':
    path = input('enter path --> ')
    if not os.path.isfile(path): exit()

    print(full_words(length, path))
