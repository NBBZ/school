def isPalindrome(n):
    """is input a palindrome?"""
    # comparing the num (in str form) to its reverse 
    # (reversing by getting the string with every "-1th charachter")
    return str(n) == str(n)[::-1]

# examples
print('q14 (Palindrome):')
x = input('--> ')
print(isPalindrome(int(x)))
print('\n')




def isTriangle(a, b, c):
    """can there be a triangle with side lengths as the inputs?"""
    # simply comparing the values and putting them in boolean variables for readability
    x = a + b >= c
    y = c + b >= a
    z = a + c >= b
    return x and y and z

# examples
print('q16 (Triangle, enter 3 nums):')
x, y, z = input('--> ')
print(isTriangle(x, y, z))
print('\n')




def fibUnderNum(n):
    """print fibonacci sequence up to input"""
    a,b,c = 0,1,1
    print(a,b,c, sep=', ', end=', ')
    while c <= n:
        print(c, end=', ')
        a, b, c = b, c, b+c

# examples
print('q6 (Fibonacci up to N):')
x = input('--> ')
print(fibUnderNum(int(x)))
print('not sure why the variable gets a null value...')
print('\n')




def bLang(string):
    """turn a string into B languege"""
    # simply going over the input and appending to the return variable as needed
    bString = ''
    for i in string:
        if i in list('aeiou'):
            bString += i + 'b' + i
        else:
            bString += i
    return bString

# examples
print('q7 (B lang):')
x = input('--> ')
print(bLang(x))
print('ani ohev madmah')
print(bLang('ani ohev madmah'))
