// Q1
public static Timer shorter (Timer t1, Timer t2){
        if((t1.getHour()*60 + t1.getMin()) > (t2.getHour()*60 + t2.getMin()))
                return t1;
        return t2;
}
public static Timer bestTime (Timer[] arr){
        Timer min = arr[0];
        for(int i = 0; i < arr.length; i++)
                min = shorter(arr[i], min); // I think thats ok...
        return min;
}

//Q2
public static int[] doubleSwitch (int[] arr){
        int arr2[] = new int[arr.length*2];
        arr2[0] = arr[0];
        int c = 0; // last pos in arr2
        for(int i = 1; i < arr.length - 1; i++)
                if((arr[i-1] > arr[i] && arr[i] < arr[i+1]) || (arr[i-1] < arr[i] && arr[i] > arr[i+1])){
                        arr2[c+1] = arr[i];
                        arr2[c+2] = arr[i];
                        c = c+2;
                }
                else{
                        arr2[c+1] = arr[i];
                        c++;
                }
        arr2[c+1] = arr[arr.length-1];
        c++;
        int returnArr[] = new int[c+1];
        for(int i = 0; i <= c; i++)
                returnArr[i] = arr2[i];
        return returnArr;
}

//Q6
public static void q6(Queue q, int c, int sum){
        if (q.isEmpty())
                return;
        x = q.remove();
        if (x > sum/c)
                System.out.println(x);
        q6(q, c+1, sum+x); 
}

//Q7
public static int lstLen(Node lst){
        Node p = lst;
        int count = 0;
        while (p != null){
                count++;
                p = p.getNext();
        }
        return count;
}

public static boolean q7(Node[] arr, int i){
        if((i+1) == arr.length)
                return true;
        if(lstLen(arr[i]) != arr.length-(i+1))
                return false;
        return q7(arr, i++);
}


public static double[] averages (int[] grades, int[] arr){
        double[] narr = new int[arr.length];
        int j = 0;
        int sum = 0;
        for (int i = 0; i < arr.length; i++){
                while(j < j+arr[i]){
                        sum += grades[j];
                        j++;
                }
                narr[i] = sum/arr[i];
                sum = 0;
        }
        return narr;
}


public void insertNum(int x){
        Node p = this.lst;
        // if empty or need to insert to first
        if (p == null || p.getValue().getNum() > x){
                lst = new Node(new NumCount(x,1), lst);
                return;
        }
        // insert to mid
        while (p.getNext() != null){
                if (p.getValue().getNum() == x){
                        p.getValue().setCount(p.getValue().getCount() + 1);
                        return;
                }
                if (p.getNext().getValue().getNum() > x){
                        p.setNext(new Node(new NumCount(x,1), p.getNext()));
                        return;
                }
        }
        // insert to end
        p.setNext(new Node(new NumCount(x,1), lst));
}



public int valueN(int n){
        int c = p.getValue().getCount();
        Node p = this.lst;
        while (c < n){
                c += p.getNext().getValue().getCount();
                p = p.getNext();
        }
        return p.getValue().getNum();   
}











public static Node build (Node lst){
        Node p = lst.getNext();
        Node newL = 





















