import unit4.collectionsLib.Node;

public class Main {
    public static int listLength(Node<Integer> lst){
        Node<Integer> p = lst;
        int len = 0;
        while (p != null){
            p = p.getNext();
            len++;
        }
        return len;
    }

    public static void printList(Node<Integer> lst){
        Node<Integer> p = lst;
        while (p != null){
            System.out.print(p.getValue() + " --> ");
            p = p.getNext();
        }
        System.out.println();
    }

    public static void addToSorted(Node<Integer> lst, int num){
        Node<Integer> p = lst;
        Node<Integer> node = new Node<Integer>(num);
        if (p.getValue() > node.getValue()) { node.setNext(p); }
        while (p.getNext().getValue() < node.getValue()) { p = p.getNext(); }
        node.setNext(p.getNext());
        p.setNext(node);
    }

    public static void addComplete(Node<Integer> lst, int num){
        Node<Integer> p = lst;
        while (p.getNext() != null) { 
                Node<Integer> node = new Node<Integer>(num - p.getValue());
                node.setNext(p.getNext());
                p.setNext(node);
                p = node.getNext(); 
        }
        Node<Integer> node = new Node<Integer>(num - p.getValue());
        p.setNext(node);
    }

    public static Node<Integer> removeDuplicates(Node<Integer> lst){
        Node<Integer> p = lst;
        Node<Integer> q;
        while (p.getNext() != null){
                if (p.getValue() == p.getNext().getValue()){
                        q = p.getNext();
                        p.setNext(p.getNext().getNext());
                        q.setNext(null);
                        p = lst;
                        continue;
                }
                p = p.getNext();
        }
        return lst;
    }

    public static int maxLocation(Node<Integer> lst){
        int maxLoc = 0;
        int maxVal = 0;
        int currLoc = 0;
        Node<Integer> p = lst;
        while (p != null) { 
                if (p.getValue() > maxVal) { 
                        maxVal = p.getValue();
                        maxLoc = currLoc;
                }
                p = p.getNext(); 
                currLoc++;
        }
        return maxLoc;
    }

    public static Node<Integer> removeNMaxNums(Node<Integer> lst, int num){
        Node<Integer> p = lst;
        Node<Integer> q;
        for (int i = 1; i <= num; i++){
            System.out.println("removing Max Num " + i);
            // hmm... doesnt remove outside of function
            if (maxLocation(lst) == 0){ 
                    lst = lst.getNext(); 
                    System.out.println("Head is: " + lst.getValue());
            }
            for (int j = 0; j < maxLocation(lst) - 1; j++){ p = p.getNext(); }
            q = p.getNext();
            p.setNext(p.getNext().getNext());
            q.setNext(null);
            //printList(lst);
            p = lst;
        }
        return lst;
    }

    public static void main(String[] args) {
/*
        Node<Integer> p12 = new Node<Integer>(4);
        Node<Integer> p11 = new Node<Integer>(3, p12);
        Node<Integer> p10 = new Node<Integer>(2, p11);
        Node<Integer> p9 = new Node<Integer>(1, p10);
        Node<Integer> p8 = new Node<Integer>(4, p9);
        Node<Integer> p7 = new Node<Integer>(3, p8);
        Node<Integer> p6 = new Node<Integer>(2, p7);
        Node<Integer> p5 = new Node<Integer>(1, p6);
        Node<Integer> p4 = new Node<Integer>(4, p5);
        Node<Integer> p3 = new Node<Integer>(3, p4);
        Node<Integer> p2 = new Node<Integer>(2, p3);
        Node<Integer> p1 = new Node<Integer>(1, p2);
        printList(p1);
        printList(removeDuplicates(p1));
        System.out.println("\n");
*/


        Node<Integer> lst12 = new Node<Integer>(12);
        Node<Integer> lst11 = new Node<Integer>(11, lst12);
        Node<Integer> lst10 = new Node<Integer>(10, lst11);
        Node<Integer> lst9 = new Node<Integer>(9, lst10);
        Node<Integer> lst8 = new Node<Integer>(8, lst9);
        Node<Integer> lst7 = new Node<Integer>(7, lst8);
        Node<Integer> lst6 = new Node<Integer>(6, lst7);
        Node<Integer> lst5 = new Node<Integer>(5, lst6);
        Node<Integer> lst4 = new Node<Integer>(4, lst5);
        Node<Integer> lst3 = new Node<Integer>(3, lst4);
        Node<Integer> lst2 = new Node<Integer>(2, lst3);
        Node<Integer> lst1 = new Node<Integer>(10, lst2);
        printList(lst1);
        //addComplete(lst1, 20);
        printList(removeNMaxNums(lst1,3));
        printList(lst1);
        System.out.println("Head is: " + lst1.getValue());
        System.out.println("\n");


/*
        Node<Integer> sub_test_lst5 = new Node<Integer>(5);
        Node<Integer> sub_test_lst4 = new Node<Integer>(3, sub_test_lst5);
        Node<Integer> sub_test_lst3 = new Node<Integer>(2, sub_test_lst4);
        Node<Integer> sub_test_lst2 = new Node<Integer>(1, sub_test_lst3);
        Node<Integer> sub_test_lst1 = new Node<Integer>(1, sub_test_lst2);
        Node<Integer> sub_test_lstwo3 = new Node<Integer>(3);
        Node<Integer> sub_test_lstwo2 = new Node<Integer>(2, sub_test_lstwo3);
        Node<Integer> sub_test_lstwo1 = new Node<Integer>(1, sub_test_lstwo2);
        printList(sub_test_lst1);
        printList(sub_test_lstwo1);
*/
    }
}
