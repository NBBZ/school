import unit4.collectionsLib.Node;

public class Student {
        String fName;
        String lName;
        double avgGrade;

        public Student(String fName, String lName, double avgGrade)
        {
            this.fName = fName;
            this.lName = lName;
            this.avgGrade = avgGrade;
        }
 
        public String getFName() { return this.fName; }
        public String getlName() { return this.lName; }
        public double getAvg() { return this.avgGrade; }

        public void setFName(String name) { this.fName = name; }
        public void setlName(String name) { this.lName = name; }
        public void setAvg(Double avg) { this.avgGrade = avg; }
 
        public String toString()
        {
            return ("First Name: " + this.fName
                    + "Last Name: " + this.lName
                    + "Avarage grade: " + this.avgGrade);
        }

        // Exrecise 3
        public void showExcelentFail(Node<Student> lst){
                Node<Student> p = lst;
                while (p != null){
                        if (p.getValue().avgGrade >= 95 || p.getValue().avgGrade <= 40)
                                System.out.println(p.getValue());
                }
        }

        // Exrecise 4
        public double listAvarage(Node<Student> lst){
                Node<Student> p = lst;
                int len = 0;
                int sum = 0;
                while (p != null){
                        len ++;
                        sum += p.getValue().avgGrade;
                        p = p.getNext();
                }
                return sum/len;
        }


        // Exrecise 5
        public static void addToSortedGrade(Node<Student> lst, Student std){
            Node<Student> p = lst;
            Node<Student> node = new Node<Student>(std);
            if (p.getValue().avgGrade > node.getValue().avgGrade) { node.setNext(p); }
            while (p.getNext().getValue().avgGrade < node.getValue().avgGrade) { p = p.getNext(); }
            node.setNext(p.getNext());
            p.setNext(node);
        }

        // Exrecise 6
        public static void addToSortedLastName(Node<Student> lst, Student std){
            Node<Student> p = lst;
            Node<Student> node = new Node<Student>(std);
            if (p.getValue().lName.compareTo(node.getValue().lName) > 0) { node.setNext(p); }
            while (p.getNext().getValue().lName.compareTo(node.getValue().lName) < 0) { p = p.getNext(); }
            node.setNext(p.getNext());
            p.setNext(node);
        }

        // Exrecise 7
        public static Node<Student> removeUnderGrade(Node<Student> lst, double num){
            Node<Student> p = lst;
            Node<Student> q;
            while (p.getNext() != null){
                    if (p.getValue().avgGrade < num){
                            q = p.getNext();
                            p.setNext(p.getNext().getNext());
                            q.setNext(null);
                            p = lst;
                            continue;
                    }
                    p = p.getNext();
            }
            return lst;
        }

        public static void main(String[] args) {
            // Exrecise 2
            Scanner sc = new Scanner(System.in);
            newL = new Node<Student>(null);
            np = newL;
            for (int i=0; i<5; i++){
                    fName = sc.next();
                    lName = sc.next();
                    avgGrade = sc.nextInt();
                    np.setNext(new Node<Student>(new Student(fName, lName, avgGrade)));
                    np = np.getNext();
            }
    }
}
