import unit4.collectionsLib.Node;

public class Main {
    public static int listLength(Node<Integer> lst){
        Node<Integer> p = lst;
        int len = 0;
        while (p != null){
            p = p.getNext();
            len++;
        }
        return len;
    }

    public static void printList(Node<Integer> lst){
        Node<Integer> p = lst;
        while (p != null){
            System.out.print(p.getValue() + " --> ");
            p = p.getNext();
        }
        System.out.println();
    }

    public static boolean triList(Node<Integer> lst){
        Node<Integer> p = lst;
        if (listLength(p) % 3 != 0){ return false; }
        Node<Integer> p2 = lst;
        Node<Integer> p3 = lst;
        for (int i = 0; i < listLength(p)/3; i++){
            p2 = p2.getNext();
            p3 = p3.getNext().getNext();
        }
        System.out.println("p1 = " + p.getValue() + ", p2 = " + p2.getValue() + ", p3 = " + p3.getValue());
        System.out.println("ENTERING LOOP");
        while (p3.getNext() != null){
            System.out.println("p1 = " + p.getValue() + ", p2 = " + p2.getValue() + ", p3 = " + p3.getValue());
            if (p2.getValue() != p3.getValue() || p2.getValue() != p.getValue()){
                return false;
            }
            p = p.getNext();
            p2 = p2.getNext();
            p3 = p3.getNext();
        }
        return true;
    }

    public static boolean dualList(Node<Integer> lst){
        Node<Integer> p = lst;
        if (listLength(p) % 2 != 0){ return false; }
        Node<Integer> p2 = lst;
        for (int i = 0; i < listLength(p)/2; i++){
            p2 = p2.getNext();
        }
        System.out.println("p1 = " + p.getValue() + ", p2 = " + p2.getValue());
        System.out.println("ENTERING LOOP");
        while (p2.getNext() != null){
            System.out.println("p1 = " + p.getValue() + ", p2 = " + p2.getValue());
            if (p.getValue() != p2.getValue()){
                return false;
            }
            p = p.getNext();
            p2 = p2.getNext();
        }
        return true;
    }

    public static boolean isArranged(Node<Integer> lst){
        Node<Integer> p = lst;
        if (listLength(p) % 2 != 0){ return false; }
        Node<Integer> half = lst;
        for (int i = 0; i < listLength(p)/2; i++){ half = half.getNext(); }
        System.out.println("half = " + half.getValue());

        Node<Integer> p2;
        while (p != half){
            System.out.println("p = " + p.getValue());
            p2 = half;
            while (p2 != null){
                System.out.println("p2 = " + p2.getValue());
                if (p.getValue() >= p2.getValue()){ return false; }
                p2 = p2.getNext();
            }
            p = p.getNext();
        }
        return true;
    }


    public static boolean isPrefix(Node<Integer> lst1, Node<Integer> lst2){
        Node<Integer> p1 = lst1;
        Node<Integer> p2 = lst2;
        if (listLength(p1) > listLength(p2)) { return false; }
        while (p1.getNext() != null){
                if (p1.getValue() != p2.getValue()) { return false; }
                p1 = p1.getNext();
                p2 = p2.getNext();
        }
        return true;
    }

    public static boolean isSubChain(Node<Integer> lst1, Node<Integer> lst2){
        Node<Integer> p1 = lst1;
        Node<Integer> p2 = lst2;
        while (p2.getNext() != null){
                if (isPrefix(p1, p2)) { return true; }
                p2 = p2.getNext();
        }
        return false;
    }

    public static int longestSequence(Node<Integer> lst){
        Node<Integer> p = lst;
        int count = 0;
        int max = 0;
        while (p.getNext() != null) {
            if (p.getNext().getValue() >= p.getValue()) { count++; }
            else { 
                    max = Math.max(max, count); 
                    count = 0;
            }
            p = p.getNext();
        }
        // if the list is one long sequence
        if (count != 0 && max == 0) { return count + 1; }
        return max + 1;
    }


    public static void main(String[] args) {
        Node<Integer> p12 = new Node<Integer>(4);
        Node<Integer> p11 = new Node<Integer>(3, p12);
        Node<Integer> p10 = new Node<Integer>(2, p11);
        Node<Integer> p9 = new Node<Integer>(1, p10);
        Node<Integer> p8 = new Node<Integer>(4, p9);
        Node<Integer> p7 = new Node<Integer>(3, p8);
        Node<Integer> p6 = new Node<Integer>(2, p7);
        Node<Integer> p5 = new Node<Integer>(1, p6);
        Node<Integer> p4 = new Node<Integer>(4, p5);
        Node<Integer> p3 = new Node<Integer>(3, p4);
        Node<Integer> p2 = new Node<Integer>(2, p3);
        Node<Integer> p1 = new Node<Integer>(1, p2);
        printList(p1);
        System.out.println("list length:");
        System.out.println(listLength(p1));
        System.out.println("is tri list:");
        System.out.println(triList(p1));
        System.out.println("is arranged list");
        System.out.println(isArranged(p1));
        System.out.println("longest seq");
        System.out.println(longestSequence(p1));
        System.out.println("\n");

        Node<Integer> lst12 = new Node<Integer>(12);
        Node<Integer> lst11 = new Node<Integer>(11, lst12);
        Node<Integer> lst10 = new Node<Integer>(10, lst11);
        Node<Integer> lst9 = new Node<Integer>(9, lst10);
        Node<Integer> lst8 = new Node<Integer>(8, lst9);
        Node<Integer> lst7 = new Node<Integer>(7, lst8);
        Node<Integer> lst6 = new Node<Integer>(6, lst7);
        Node<Integer> lst5 = new Node<Integer>(5, lst6);
        Node<Integer> lst4 = new Node<Integer>(4, lst5);
        Node<Integer> lst3 = new Node<Integer>(3, lst4);
        Node<Integer> lst2 = new Node<Integer>(2, lst3);
        Node<Integer> lst1 = new Node<Integer>(1, lst2);
        printList(lst1);
        System.out.println("list length:");
        System.out.println(listLength(lst1));
        System.out.println("is tri list:");
        System.out.println(triList(lst1));
        System.out.println("is arranged list");
        System.out.println(isArranged(lst1));
        System.out.println("longest seq");
        System.out.println(longestSequence(lst1));
        System.out.println("\n");

        Node<Integer> sub_test_lst5 = new Node<Integer>(5);
        Node<Integer> sub_test_lst4 = new Node<Integer>(3, sub_test_lst5);
        Node<Integer> sub_test_lst3 = new Node<Integer>(2, sub_test_lst4);
        Node<Integer> sub_test_lst2 = new Node<Integer>(1, sub_test_lst3);
        Node<Integer> sub_test_lst1 = new Node<Integer>(1, sub_test_lst2);
        Node<Integer> sub_test_lstwo3 = new Node<Integer>(3);
        Node<Integer> sub_test_lstwo2 = new Node<Integer>(2, sub_test_lstwo3);
        Node<Integer> sub_test_lstwo1 = new Node<Integer>(1, sub_test_lstwo2);
        printList(sub_test_lst1);
        printList(sub_test_lstwo1);
        System.out.println("is prefix");
        System.out.println(isPrefix(sub_test_lstwo1, sub_test_lst1));
        System.out.println("is sub chain");
        System.out.println(isSubChain(sub_test_lstwo1, sub_test_lst1));
    }
}
