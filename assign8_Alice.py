import re
import os
def popularWords(file_path):
    # checking if file exists
    if not os.path.isfile(file_path):
        print('SOURCE FILE OR DESTINATION FILE DOES NOT EXIST')
        return
    source = open(file_path, 'r')
    # removing punctuation using regex i dont understand :(
    text = re.sub(r'[^\w\s]', '', source.read()).lower()
    wordList = text.split()
    wordDict = {}
    for word in set(wordList):
        wordDict[word] = wordList.count(word)
    sortedWords = sorted(wordDict.items(), key=lambda x:x[1], reverse=True)
    for i in range(20): print(sortedWords[i])
    source.close()

popularWords('./alice.txt')
