########    ###    ##
   ##      ## ##   ##
   ##     ##   ##  ##
   ##    ##     ## ##
   ##    ######### ##
   ##    ##     ## ##
   ##    ##     ## ########


##       ######## ######## ########   #######  ##    ##
##       ##       ##       ##     ## ##     ## ###   ##
##       ##       ##       ##     ## ##     ## ####  ##
##       ######   ######   ########  ##     ## ## ## ##
##       ##       ##       ##   ##   ##     ## ##  ####
##       ##       ##       ##    ##  ##     ## ##   ###
######## ######## ######## ##     ##  #######  ##    ##


##    ## ####  ######     ###    ##    ##
###   ##  ##  ##    ##   ## ##   ###   ##
####  ##  ##  ##        ##   ##  ####  ##
## ## ##  ##   ######  ##     ## ## ## ##
##  ####  ##        ## ######### ##  ####
##   ###  ##  ##    ## ##     ## ##   ###
##    ## ####  ######  ##     ## ##    ##


# Cyber Comp 2.0 - 4.1.2024
# Tal Nisan and Leeron

import requests
from bs4 import BeautifulSoup
year = input('enter year: ')
if not str.isnumeric(year):
    exit()
if int(year) < 1980:
    exit()
url = f'https://www.imdb.com/search/title/?title_type=feature&release_date=' + year + '-01-01,' + year + '-12-31&sort=user_rating,desc'

headers = {'User-Agent': 'Opera/5.0 (Windows NT 11.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'}

response = requests.get(url, headers=headers)

soup = BeautifulSoup(response.text, "html.parser")
quote = soup.find_all("h3", class_="ipc-title__text")
for i in quote:
    print(str(i)[28:-5])
    if str(i)[29:30] == '0': break

