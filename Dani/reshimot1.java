public class LinkedLists {

    public static int listSum(Node<Integer> lst) {
        int sum = 0;
        Node<Integer> p = lst;
        while (p != null) {
            sum += p.getValue();
            p = p.getNext();
        }
        return sum;
    }

    public static int listSumOdd(Node<Integer> lst) {
        int sum = 0;
        int count = 1;
        Node<Integer> p = lst;
        while (p != null) {
            if (count % 2 == 1) {sum += p.getValue();}
            p = p.getNext();
            count++;
        }
        return sum;
    }

    public static boolean isInList(Node<Integer> lst, int n) {
        Node<Integer> p = lst;
        while (p != null) {
            if (p.getValue() == n) {return true;}
            p = p.getNext();
        }
        return false;
    }

    public static int numInListCount(Node<Integer> lst, int n) {
        int count = 0;
        Node<Integer> p = lst;
        while (p != null) {
            if (p.getValue() == n) {count++;}
            p = p.getNext();
        }
        return count;
    }

    public static boolean isSorted(Node<Integer> lst) {
        Node<Integer> p = lst;
        while (p.getNext() != null) {
            if (p.getNext().getValue() < p.getValue()) {return false;}
            p = p.getNext();
        }
        return true;
    }

    public static boolean isArithmeticSeq(Node<Integer> lst) {
        int d = p.getNext().getValue() - p.getValue();
        Node<Integer> p = lst;
        while (p.getNext() != null) {
            if (p.getNext().getValue() - p.getValue() != d) {return false;}
            p = p.getNext();
        }
        return true;
    }
}
