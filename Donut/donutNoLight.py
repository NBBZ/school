# {{{ unimportant
import curses
import math
import time
stdscr = curses.initscr()
curses.noecho() # dont echo the typed char to screen
curses.curs_set(False) # hide curser
#screen_width, screen_height = 100, 100
screen_width = stdscr.getmaxyx()[1]
screen_height = stdscr.getmaxyx()[0]
# }}} unimportant

# {{{ Variables
# how much should THETA increment
cross_section_resolution = 0.07

# how much should PHI increment
donut_radius_resolution = 0.02

# R1 (the radius of the circle we are rotating to create the donut)
thickness = 1

# R2 (the radius of the rotation of said donut)
radius = 2

# K2
donut_distance = 5

# K1 / Z'
screen_distance = screen_width*donut_distance*3/(8*(thickness+radius))
# }}} Variables

def renderFrame(A, B):
    cosA, sinA = math.cos(A), math.sin(A)
    cosB, sinB = math.cos(B), math.sin(B)

    phi = 0
    # phi goes around the donut radius
    while phi < math.pi*2:
        theta = 0
        cosPHI, sinPHI = math.cos(phi), math.sin(phi)
        # theta goes around the cross section
        while theta < math.pi*2:
            cosTHETA, sinTHETA = math.cos(theta), math.sin(theta)

            # cords on the circle before any rotations
            circleX = radius + thickness * cosTHETA
            circleY = thickness * sinTHETA

            # the 3D cords on the donut after rotating (just the sum of all the matrecies)
            x = circleX*(cosB*cosPHI + sinA*sinB*sinPHI) - circleY*cosA*sinB 
            y = circleX*(sinB*cosPHI - sinA*cosB*sinPHI) + circleY*cosA*cosB
            z = donut_distance + cosA*circleX*sinPHI + circleY*sinA

            projectionX = int((screen_distance*x / z)/3 + screen_width/2)
            projectionY = int((screen_distance*y / z)/6 + screen_height/2)

            stdscr.addstr(projectionY, projectionX, '@')

            theta += cross_section_resolution

        phi += donut_radius_resolution


rot_axis_A ,rot_axis_B = 0, 0

t_end = time.time() + 15
while time.time() < t_end:
    renderFrame(rot_axis_A, rot_axis_B)
    stdscr.refresh()
    rot_axis_A += 0.07
    rot_axis_B += 0.07
    stdscr.clear()


# curses <3
curses.nocbreak()
stdscr.keypad(False)
curses.echo()
curses.endwin()
