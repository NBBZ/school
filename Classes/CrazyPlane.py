import random
class CrazyPlane:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.dirArr = [(1,0) , (1,1) , (0,1) , (-1,1) , (-1,0) , (-1,-1) , (0,-1) , (1,-1)]

    def __str__(self):
        return str(self.x + 1) + ' , ' + str(self.y + 1)

    def updatePos(self):
        self.x += random.randint(-1,1)
        self.y += random.randint(-1,1)
        self.x = self.x % 10
        self.y = self.y % 10

    def move(self, dir):
        x = self.x+self.dirArr[dir][0]
        y = self.y+self.dirArr[dir][1]
        x = x % 10
        y = y % 10
        return x,y

    def isClose(self, x, y):
        #if self.x == x and self.y == y: return True
        closeX = abs(self.x - x) <= 2
        closeY = abs(self.y - y) <= 2
        if closeX and closeY: return True
        borderX = (self.x == 0 and x == 9) or (self.x == 9 and x == 0)
        borderY = (self.y == 0 and y == 9) or (self.y == 9 and y == 0)
        if (borderX and closeY) or (borderY and closeX): return True
        return False
