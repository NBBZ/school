import curses
import time
stdscr = curses.initscr()
screen_width = stdscr.getmaxyx()[1]
screen_height = stdscr.getmaxyx()[0]

from Classes.CrazyPlane import *
#planeArr = [CrazyPlane(0,0), CrazyPlane(9,0), CrazyPlane(0,9), CrazyPlane(9,9)]
planeArr = [CrazyPlane(0,0), CrazyPlane(9,9)]

# goes over all the available moving locations (plane.move(0...7)) and if one of them is close to a plane then return False
def canRandom(plane):
    for i in range(8):
        for plane2 in planeArr:
            if plane == plane2: pass
            if plane2.isClose(plane.move(i)[0], plane.move(i)[1]): return False
    return True

def moveToAvil(plane):
    for i in range(8):
        for plane2 in planeArr:
            if plane == plane2: pass
            if not plane2.isClose(plane.move(i)[0], plane.move(i)[1]):
                plane.x, plane.y = plane.move(i)[0], plane.move(i)[1]
                return True
    return False

def drawFrame():
    # Planes
    for i in range(2): stdscr.addstr(planeArr[i].x + 1, planeArr[i].y + 1, str(i))
    # Frame
    for x in range(11):
        stdscr.addstr(x, 0, '#')
        stdscr.addstr(x, 11, '#')
        stdscr.addstr(0, x, '#')
        stdscr.addstr(11, x, '#')
    # "Framerate"
    time.sleep(0.08)
    stdscr.refresh()
    stdscr.clear()
    #for i in range(2):
    #    print('plane: ', i, 'pos: ', planeArr[i].x, planeArr[i].y)


# MAIN LOOP
for i in range(100):
    for plane in planeArr:
        if canRandom(plane): 
            plane.updatePos()
        else: 
            moveToAvil(plane)
            #stdscr.addstr(11, 11, 'NOT RANDOM')
            #stdscr.refresh()
            #time.sleep(0.01)
        drawFrame()




# curses <3
curses.nocbreak()
stdscr.keypad(False)
curses.echo()
curses.endwin()
