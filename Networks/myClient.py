import socket
import protocol

def main():
    mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    mySocket.connect(('127.0.0.1', protocol.PORT))

    while True:
        userInput = input('Enter command\n')
        # Check if user entered a valid command as defined in protocol
        validCmd = protocol.check_cmd(userInput)

        if not validCmd:
            print('Not a valid command')
            pass
        mySocket.send(protocol.create_msg(userInput))
        if userInput == 'EXIT': break
        validResp, serverResponse = protocol.get_msg(mySocket)
        if not validResp:
            print('Bad server response')
            mySocket.recv(1024)
            pass
        print(serverResponse)


    print('Closing\n')
    # Close socket


if __name__ == '__main__':
    main()
