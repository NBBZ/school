import socket
'''EX 2.6 protocol implementation
   Author: Nisan Perelman
   Date: 24.12
'''

LENGTH_FIELD_SIZE = 4
PORT = 8830
validCmd = ['RAND', 'TIME', 'EXIT', 'WHORU', 'HELP', 'DIR', 'DELETE', 'COPY', 'EXECUTE']

def check_cmd(cmd: str):
    '''Check if the command is defined in the protocol'''
    return cmd.split()[0] in validCmd

def create_msg(messege: str) -> bytes:
    '''Create a valid protocol message, with length field'''
    msg = str(len(messege)).zfill(LENGTH_FIELD_SIZE) + messege # add leading zeros
    return msg.encode()


def get_msg(my_socket: socket.socket):
    '''Extract message from protocol, without the length field
       If length field does not include a number, returns False, 'Error' '''
    len = my_socket.recv(LENGTH_FIELD_SIZE)
    if not len.decode().isnumeric(): return False, 'Error'
    return True, my_socket.recv(int(len.decode())).decode()
