import socket
import protocol
import datetime
import random
import glob
import os
import shutil
import subprocess

def create_server_rsp(cmd) -> str:
    '''Based on the command, create a proper response'''
    if cmd == 'HELP': return str(protocol.validCmd)
    if cmd == 'TIME': return str(datetime.datetime.now())
    if cmd == 'WHORU': return 'I am NBBZ'
    if cmd == 'RAND': return str(random.randint(0,10))

    if cmd.split()[0] == 'DIR' and not os.path.isdir(cmd.split()[1]) and len(cmd.split()) != 2: return 'BAD DIR ARGUMENT'
    if cmd.split()[0] == 'DIR': return str(glob.glob(cmd.split()[1] + '/*'))

    if cmd.split()[0] == 'DELETE' and (not os.path.isfile(cmd.split()[1]) or len(cmd.split()) != 2): 
        return 'BAD DELETE ARGUMENT'
    if cmd.split()[0] == 'DELETE': 
        os.remove(cmd.split()[1])
        return 'REMOVED FILE ' + cmd.split()[1]

    if cmd.split()[0] == 'COPY' and (not os.path.isfile(cmd.split()[1]) or not os.path.isfile(cmd.split()[2]) or len(cmd.split()) != 3): 
        return 'BAD COPY ARGUMENT'
    if cmd.split()[0] == 'COPY': 
        shutil.copy(cmd.split()[1], cmd.split()[2])
        return 'COPIED FILE' + cmd.split()[1] + 'TO ' + cmd.split()[2]

    if cmd.split()[0] == 'EXECUTE' and (not os.path.isfile(cmd.split()[1]) or len(cmd.split()) != 2): 
        return 'BAD EXEC ARGUMENT'
    if cmd.split()[0] == 'EXECUTE': 
        subprocess.call(cmd.split()[1])
        return 'EXECUTED FILE ' + cmd.split()[1]

    return 'ERROR'


def main():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(('0.0.0.0', protocol.PORT))
    server_socket.listen()
    print('Server is up and running')
    (client_socket, client_address) = server_socket.accept()
    print('Client connected')

    while True:
        # Get message from socket and check if it is according to protocol
        valid_msg, cmd = protocol.get_msg(client_socket)
        if not valid_msg:
            client_socket.send(protocol.create_msg('Wrong protocol'))
            client_socket.recv(1024)  # Attempt to empty the socket from possible garbage
            pass
        if not protocol.check_cmd(cmd):
            client_socket.send(protocol.create_msg('Invalid command'))
            pass
        print(cmd)
        if cmd == 'EXIT': break
        client_socket.send(protocol.create_msg(create_server_rsp(cmd)))

    print('Closing\n')
    client_socket.close()
    server_socket.close()


if __name__ == '__main__':
    main()
