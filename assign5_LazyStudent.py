import os
import sys
def copy_paste(file_path, dir):
    if not os.path.isfile(file_path) or not os.path.isdir(dir): # checking if the file and the destination exist
        print('EITHER FILE OR DESTINATION DIR DOES NOT EXIST')
        return
    file_source = open(file_path, 'rb')                         # opening the file we want to copy in binary mode, thats ok because were just dumping the whole file in
    file_name = os.path.basename(os.path.normpath(file_path))   # getting the last element of the input file path (the file name itself)
    with open(dir + '/' + file_name, 'wb') as copied:           # opening (creating) the file in the targer directory (I'm assuming the dir is in '/foo/bar/dir' format and not '/foo/bar/dir/' because thats the output of pwd)
        copied.write(file_source.read())
    file_source.close()


def lazy_student(source_path, solutions_path):
    if not os.path.isfile(source_path) or not os.path.isfile(solutions_path): # checking if the files exist
        print('SOURCE FILE OR DESTINATION FILE DOES NOT EXIST')
        return
    source = open(source_path, 'r')
    solutions = open(solutions_path, 'a')
    # going over the lines of the source, if the line is a valid expression, write to the solutions file the line + " = <solution>"
    # if its not a real expression, write "ERROR"
    for line in source:
        try:
            #print(line.strip() + ' = ' + str(eval(line)))
            solutions.write(line.strip() + ' = ' + str(eval(line)) + '\n')
        except:
            #print('ERROR')
            solutions.write('ERROR\n')
    source.close()
    solutions.close()


if len(sys.argv) == 3:
    source_path, solutions_path = sys.argv[1], sys.argv[2] # getting command-line args
    lazy_student(source_path, solutions_path)
else:
    print('ENTER ARGS (source file and solution file)')
