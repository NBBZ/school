ex1 = lambda string : ''.join(map(lambda char : char*2, string))

ex2_not_working = lambda num : list(map(lambda n : n if n%4==0 else None, list(range(0, num))))
# ^ Im sure this way can work too somehow but i couldnt find a way to make it work
# It returns [0, None, None, None, 4, None, None....

ex2 = lambda num : list(filter(lambda n : not n%4, range(0, num)))

ex3 = lambda num : sum(list(map(int, str(num))))

ex4 = lambda arr1, arr2 : list(set([x for x in arr2 if x in arr2 and x in arr1] + [x for x in arr1 if x in arr2 and x in arr1]))
# *remembers set theory*
ex4 = lambda arr1, arr2 : list(set([x for x in arr1 if x in arr2 and x in arr1]))

ex5 = lambda arr1, arr2 : sum(list(map(lambda tup : abs(tup[0] - tup[1]) , zip(arr1, arr2)))) / len(arr1)

ex6 = lambda arr : [x.strip() for x in arr]
